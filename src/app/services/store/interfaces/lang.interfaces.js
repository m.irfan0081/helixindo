export default {
    id: {
        information: {
            phone: '+6281 1101 4393',
            email: 'sales@helixindo.com',
            startOpen_Day: 'Senin',
            startOpen_time: '08:00',
            endOpen_Day: 'Jumat',
            endOpen_Time: '16:00',
            address: 'Kota Tangerang Selatan, Banten 15325',
            inf: 'Kami adalah perusahaan instalasi utama Sistem Tenaga Surya yang berbasis di Indonesia. Helix Solar Power Systems dan layanan dirancang khusus untuk memberikan pengalaman yang cepat, mudah, dan tanpa khawatir.'
        },
        homepage: 
        {
            header: 
            {
                
                mainMenu: [
                    {
                        text: 'Tentang Kami',
                        name: 'home.page',
                        submenu: [
                            {
                                text: 'Profil',
                                name: ''
                            },
                            {
                                text: 'Partner',
                                name: ''
                            }
                        ]
                    },
                    {
                        text: 'Layanan Kami',
                        name: '',
                        submenu: [
                            {
                                text: 'Konsultasi',
                                name: '',
                            },
                            {
                                text: 'Pelatihan',
                                name: '',
                            },
                            {
                                text: 'Instalasi',
                                name: '',
                            },
                            {
                                text: 'Dukungan',
                                name: ''
                            },
                            {
                                text: 'Pemantauan',
                                name: ''
                            },
                            {
                                text: 'Pemeliharaan',
                                name: ''
                            }
                        ]
                    },
                    {
                        text: 'Berita',
                        name: '',
                        submenu: []
                    },
                    {
                        text: 'Hubungi Kami',
                        name: '',
                        submenu: []
                    }
                ]
            },
            services: [
                {
                    text: 'Konsultasi',
                    desc: 'Kami akan membantu anda dalam mencari solusi dari kendala power energi yang anda miliki',
                    name: '',
                },
                {
                    text: 'Pelatihan',
                    desc: 'Kami menyediakan pelatihan tentang energi terbarukan',
                    name: '',
                },
                {
                    text: 'Instalasi',
                    desc: '',
                    name: '',
                },
                {
                    text: 'Dukungan Setelah Pembelian',
                    desc: '',
                    name: ''
                },
                {
                    text: 'Pemantauan',
                    desc: '',
                    name: ''
                },
                {
                    text: 'Pemeliharaan',
                    desc: '',
                    name: ''
                }
            ]
        },
        expore: {
            title: 'Projek Kami',
            subtitle: 'Kami Menyelesaikan Beberapa Projek',
            menus: ['Semua', 'Rooftop', 'PLTS', 'BTS', 'PJUTS', 'Pompa Air']
        }
    }
}