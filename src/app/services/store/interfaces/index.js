
import * as PageInterface from './page.interfaces'
import * as FaqInterface from './faq.interfaces'
import * as ProjectInterface from './project.interfaces'

export let states = {
    onLoad: false,
    lang: 'id',
    isPreloader: false, 
    ...PageInterface.default,
    ...FaqInterface.default,
    ...ProjectInterface.default
}