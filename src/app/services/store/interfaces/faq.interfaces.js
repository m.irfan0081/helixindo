export default {
    faq_en: [
        {
            question: 'What is Solar Energy and Solar Power ?',
            innerAnswere: `
                <p>
                    <b>Solar Energy </b>is what the sun produces light and heat.
                    <b>Solar Power </b>is the electricity that is processed from <b>solar energy.</b>
                </p>
            `,
            link: ''
        },
        {
            question: 'How do Solar Panels Work ?',
            innerAnswere: `
                <p>
                Solar panels contain photovoltaic cells that when exposed to light generates a flow of DC current electricity.
                </p>
            `,
            link: ''
        },
        {
            question: 'How does Solar Power Electricity Work ?',
            innerAnswere: `
                <p>
                Sunlight is absorbed by solar panels and processed into DC electricity current. Inverter and Controllers stabilize the current coming from panels and convert it into AC electricity current. AC current can be connected to your building, water pump, lights and anything to power your electricity needs.
                </p>
            `,
            link: 'about.us'
        },
        {
            question: 'Where can I Install Solar Panels or the Systems ?',
            innerAnswere: `
                <p>
                    Solar panels can be put on your buildings, garage as carports, boats, space satellites and basically anywhere there is sun! And it can power anything.
                </p>
            `,
            link: ''
        },
        {
            question: 'Why Should I Use Solar Power System ?',
            innerAnswere: `
                <p>
                Solar power is very clean and good for our earth. By using solar power instead of conventional (Coal and Oil) electricity you stop the production of air pollution that leads to global warming and natural disasters. It is a system that pays for itself and may even earn you electricity credits in the long term
                </p>
            `,
            link: ''
        },
        {
            question: 'Will My Solar Panels Work in Cloudy or Rainy Days ?',
            innerAnswere: `
                <p>
                Yes, it will! Solar panels work even there is indirect sunlight. Rain can actually be good to clean the dust off your panels for another day of more optimized performance. There is also the polycrystalline type of solar panels that is suitable for cloudy areas.
                </p>
            `,
            link: ''
        },
        {
            question: 'What about at Night ?',
            innerAnswere: `
                <p>
                Solar panels stop working when there is no light, so it does not produce electricity at night time or when there is covered by shadow. Alternatively, you can use batteries to store and reuse the sunlight produced by solar panels at night.
                </p>
            `,
            link: ''
        },
        {
            question: 'Does Having a Solar System Mean No More Power Outage When PLN is Down ?',
            innerAnswere: `
                <p>
                It depends on the type of solar power system. If you have a purely on-grid system that is connected to PLN, you have to turn off your solar system when the PLN is having power outage for safety reasons. Prevent black outs by using Hybrid or Off-grid systems.
                </p>
            `,
            link: 'about.us'
        },
        {
            question: 'Tell Me More About the Different Types of Solar Power Systems (On-Grid, Off Grid, and Hybrid) !',
            innerAnswere: `
                <p>
                On-Grid systems are connected to the PLN where they can also buy your electricity. Off-grid systems are independent and not connected to the PLN using other forms of electricity backup. Hybrid systems can be On-Grid or Off-Grid but has a combination of electricity back up systems. 
                </p>
            `,
            link: 'about.us'
        },
        {
            question: 'I Live on an Island, Will Helix Service Me ?',
            innerAnswere: `
                <p>
                I Live on an Island, Will Helix Service Me?	Of course, we will. We serve all of Indonesia and will get you a system best suited to your needs.	Saya tinggal di pulau terpencil, apakah Helix tetap bisa melayani saya?		Tentu saja, bisa. Kami melayani seluruh Indonesia dan akan memberikan Anda sistem yang paling sesuai dengan kebutuhan Anda.
                </p>
            `,
            link: ''
        },
        {
            question: 'Is Solar Power or PLN Cheaper ?',
            innerAnswere: `
                <p>
                Solar Power is cheaper in the long run, but can feel expensive because of the upfront investment model. A system generates 25 years and more of free electricity, while PLN electricity prices continually rises. On average a system install will have payed for it self in about 5-7 years, even faster if you take into account PLN prices rises and if you sell your excess electricity generation to PLN.  
                </p>
            `,
            link: 'about.us'
        },
        {
            question: 'I Found Some Cheaper Solar Panels Online, Why is That ?',
            innerAnswere: `
                <p>
                Many cheap solar panels use B or lower rated Solar/Photovoltaic Cells that drastically drops its sunlight to electricity conversion after a year of use. C- rated solar cells are like the ones you sometimes have on calculators. We only use A-quality Solar Cells that are certified, tested, and warrantied, to make sure your systems stay efficient even after years of use. Unfortunately, it is impossible for human eye to differentiate between A, B or C quality solar cells, you would need a testing machine and proper certifications
                </p>
            `,
            link: ''
        },
        {
            question: 'What is The Life Span of My System ?',
            innerAnswere: `
                <p>
                Helix only use quality products so our systems can produce electricity for 25 years and more, so you don't need to worry!
                </p>
            `,
            link: ''
        },
        {
            question: 'What Are Helix Warranty Policies ?',
            innerAnswere: `
                <p>
                Helix use solar powered systems only use certified components, each with different warranty time limit. Panels have warranty of about 20 years, Inverters for 10 years, and batteries for 5 years. We also help you if you have claims
                </p>
            `,
            link: ''
        },
        {
            question: 'How Much is a Typical Solar Power System ?',
            innerAnswere: `
                <p>
                The price of a system depends on the power installed and type of system (On-Grid, Off-Grid, Hybrid). Our consultants can help you with great pricing deals, reach out to us for a quick response to your question. Office: (021)53170106 Phone: (+62) 877 2000 0102 , Email: Solar@helixindo.com
                </p>
            `,
            link: ''
        },
        {
            question: 'Does Helix Provide Solar Financing Options ?',
            innerAnswere: `
                <p>
                Yes, we do! We emphasize on making solar powered technology accessible and will find a payment plan that works
                </p>
            `,
            link: ''
        },
        {
            question: 'How Do I Switch to Solar Power ?',
            innerAnswere: `
                <p>
                It's simple!  You can start by contacting us and we will help you make a smooth transition. We will help you complete all the paper works and reporting required for the project
                </p>
            `,
            link: ''
        },
        {
            question: 'How Long is the Process of Installing a Solar System ?',
            innerAnswere: `
                <p>
                Solar power system Installation is relatively fast, with about 2-5 day for install on houses. However, the whole process can take about 1-2 months process due to mandatory reporting to PLN, and gathering and testing of the components
                </p>
            `,
            link: ''
        },
        {
            question: 'What Do I Need to Prepare for a Solar System Install ?',
            innerAnswere: `
                <p>
                    Our consultants can take care of your preparation needs but we highly recommended that you prepare these to speed up the process even faster :
                    <ul>
                        <li>Your recent electricity bills</li>
                        <li>Your recent electricity bills</li>
                        <li>Picture of roof or site</li>
                    </ul>
                </p>
            `,
            link: ''
        },
        {
            question: 'How Do I Start Selling My Electricity to PLN ?',
            innerAnswere: `
                <p>
                Only systems connected to PLN that has Export-Import (EXIM) Meter can sell to PLN, selling to PLN means exporting electricity. To apply for EXIM meter Homeowners or Installer must submit a request to the area's PLN. Helix will help you with this with all our installs. 
                </p>
            `,
            link: ''
        },
        {
            question: 'How Much is PLN Paying for My Electricity Sales/Exports ?',
            innerAnswere: `
                <p>
                PLN will pay your electricity export 65% of the price you import/buy from the PLN. The exports are turned to credits that can be accumulated and used to deduct your electricity imports over 3-month cycles
                </p>
            `,
            link: 'about.us'
        },
        {
            question: 'What are the Maintenance Needs of a Solar Power System ?',
            innerAnswere: `
                <p>
                Solar power systems are actually very low maintenance items since they have no moving parts. There is just the occasional rinsing of the panels during dry seasons to wash away dust that hinders sunlight from hitting the panels
                </p>
            `,
            link: ''
        },
        {
            question: 'Does Helix Solar Power Systems Include Maintenance ?',
            innerAnswere: `
                <p>
                All Helix systems includes maintenance and solar training. In addition to that, we provide monitoring for selected Solar Power Systems
                </p>
            `,
            link: ''
        },
        {
            question: 'What Happens If I Want to Move Houses ?',
            innerAnswere: `
                <p>
                Helix provides relocation of your solar power system with a small fee
                </p>
            `,
            link: ''
        },
        {
            question: 'Can I Upgrade My Systems ?',
            innerAnswere: `
                <p>
                Sure, our experts can help you. Solar systems have a plug and play model that makes them flexible if you want to install more Watt panel install
                </p>
            `,
            link: ''
        },
        {
            question: 'I Still Have More Questions. What Do I Do ?',
            innerAnswere: `
                <p>
                Contact us, and an expert will help you with your questions shortly.
                </p>
            `,
            link: 'contact.us'
        },
    ]
}