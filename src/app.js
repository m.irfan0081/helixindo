import Vue from "vue";
import VueRouter from "vue-router";
import AppMain from "@/app/pages/MainPage.html";
import store from '@/app/services/store/index.js'
import VueMeta from "vue-meta"

// START PAGES
import HomePage from "@/app/pages/HomePage.html"
import ServicePage from "@/app/pages/ServicePage.html"
import ConstructionPage from "@/app/pages/ConstructionPage.html"
import TrainingPage from "@/app/pages/TrainingPage.html"
import InstallationPage from "@/app/pages/InstallationPage.html"
import AfterSalesPage from "@/app/pages/AftersalePage.html"
import RemoteMonitoringPage from "@/app/pages/RemoteMonitoringPage.html"
import MaintenancePage from "@/app/pages/MaintenancePage.html"
import FaqPage from "@/app/pages/FaqPage.html"
import PartnerList from "@/app/pages/PartnerList.html"
import ProjectList from "@/app/pages/ProjectList.html"
import AboutUs from "@/app/pages/AboutUsPage.html"
import ProjectDetail from "@/app/pages/DetailProjectPage.html"

import Calculator from "@/app/pages/CalculatorPage.html"
// END PAGES

// START IMPORT
// END IMPORT

Vue.use(VueRouter);
Vue.use(VueMeta,  {
  keyName: 'head'
});


const ROOT_PATH = "/";

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
          path: ROOT_PATH,
          component: {
            template: "<router-view></router-view>"
          },
          children: [
            {
              path: "",
              component: AppMain,
              children: [
                {
                  path: "/",
                  name: 'home.page',
                  component: HomePage
                },
                {
                  path: 'service',
                  name: 'service.page',
                  component: ServicePage
                },
                {
                  path: 'service/solar-construction',
                  name: 'construction.page',
                  component: ConstructionPage
                },
                {
                  path: 'service/solar-training',
                  name: 'training.page',
                  component: TrainingPage
                },
                {
                  path: 'service/solar-installation',
                  name: 'installation.page',
                  component: InstallationPage
                },
                {
                  path: 'service/after-sales-services',
                  name: 'aftersales.page',
                  component: AfterSalesPage
                },
                {
                  path: 'service/remote-monitoring',
                  name: 'remote.monitoring.page',
                  component: RemoteMonitoringPage
                },
                {
                  path: 'service/opertaion-maintenance',
                  name: 'maintenance.page',
                  component: MaintenancePage
                },
                {
                  path: 'faq',
                  name: 'faq.page',
                  component: FaqPage
                },
                {
                  path: 'partner',
                  name: 'parner.list',
                  component: PartnerList
                },
                {
                  path: 'project',
                  name: 'project.list',
                  component: ProjectList
                },
                {
                  path: 'about-us',
                  name: 'about.us',
                  component: AboutUs
                },
                {
                  path: 'project/:slug',
                  name: 'project.detail',
                  component: ProjectDetail,
                  props: true
                },
                {
                  path: 'calculator',
                  name: 'calculator.page',
                  component: Calculator
                }
                
              ]
            },
          ]
        }
    ]
})


var app = new Vue({
    el: "#app",
    router: router,
    store,
    metaInfo: {
      title: 'Helix Tenaga Surya'
    },
    mounted() {
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        
        // ga('create', 'UA-XXXXX-Y', 'auto');
        ga('create', 'UA-139847485-1', 'auto');
        ga('send', 'pageview');
    },
    data() {
      return {
        base_url: path => ROOT_PATH + "/" + (path || ""),
        asset: path => ROOT_PATH + "assets/" + path,
        activeLink: "home"
      };
    },
    
    
  });
